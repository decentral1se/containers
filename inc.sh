#!/bin/bash

set -e

registry="registry.git.coop"
base="$registry/bbb/containers/images"

build() {
  local name=$1;
  local version=$2;
  local image=$base/$name:$version;
  (cd "$name" && docker build -t "$image" .)
}

push() {
  local name=$1;
  local version=$2;
  local image=$base/$name:$version;
  docker push "$image"
}

ensure-logged-in() {
  if [ "x" != "x$CI_BUILD_TOKEN" ]; then
    echo "Logging into docker registry"
    docker login -u gitlab-ci-token -p "$CI_BUILD_TOKEN" $registry
  else
    # docker config file contains a json structure of all logged in registry sessions
    config=$HOME/.docker/config.json
    if [ -f "$config" ]; then
      if ! node -e "process.exit(require('$config').auths['$registry'] ? 0 : 1)"; then
        echo "Logging you in to $registry"
        docker login $registry
      fi
    else
      echo "Could not log you in, no file at $config"
    fi
  fi
}

