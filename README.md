# Docker Containers

The Docker containers which are automatically built from this repo, using GitLab CI, are designed for use by GitLab CI on [git.coop](https://git.coop/).

## BBB 

This image is designed to be used by GitLab CI for running Ansible playbooks to test [BigBlueButton](https://bigbluebutton.org/) installs.

The `registry.git.coop/meet/containers/images/bbb:0.1` image is [Ubuntu](https://hub.docker.com/_/ubuntu) Xenial plus [Ansible 2.9.9](https://github.com/ansible/ansible/blob/stable-2.9/changelogs/CHANGELOG-v2.9.rst#release-summary), `molcule` and `ansible-lint` installed using `pip3`.
